// Task 3:
//     - Write a multi-threaded program that will calculate array `b` from array `a`
//     - `a` must be an array with 100'000 elements for which `a[i] = i`
//     - calculate `b` to be the array with elements `b[i] = (a[i - 1] * a[i] * a[i + 1]) / 3.0`
//     - bonus:
//         - `b` is `a`, do not create additional arrays. original one must be mutated.
// note:
//     - for `b[0]` and `b[100'000 - 1]` do nothing as their calculation will break boudaries
// Comment: bonus task is done!
// Author: Gazizov Yakov

#include <cstdio>
#include <cstdint>
#include <array>
#include <memory>
#include <limits>
#include <utility>

#include <omp.h>

constexpr auto NUM_ELEMENTS = 100'000;

using ArrayType = std::array<double, NUM_ELEMENTS>;

constexpr ArrayType GetA() noexcept
{
    auto array = ArrayType{};
    for (auto i = 0; i < array.size(); ++i) {
        array[i] = i;
    }
    return array;
}

struct Task {
public:
    int begin = -1;
    int end = -1;

    double beforeBegin = std::numeric_limits<double>::quiet_NaN();
    double afterEnd = std::numeric_limits<double>::quiet_NaN();
};

int main([[maybe_unused]] int const argc, [[maybe_unused]] char const *argv[])
{
    // set dynamic to false just in case if implementation will allocate different number of threads
    // on every other run
    omp_set_dynamic(static_cast<int>(false));

    // output maximum available number of threads
    auto const maxThreads = omp_get_max_threads();
    std::printf("Max threads %d\n", maxThreads);

    // get the `a` array
    auto a = GetA();

    // create tasks for thread.
    // to avoid data races cache `beforeBegin` and `afterEnd` into task
    auto tasks = std::make_unique<Task[]>(maxThreads);
    auto const rangeForThread = (NUM_ELEMENTS - 2) / maxThreads;
    for (auto i = 0; i < maxThreads; ++i) {
        tasks[i].begin = 1 + rangeForThread * i;
        tasks[i].end = 1 + rangeForThread * (i + 1);

        tasks[i].beforeBegin = a[tasks[i].begin - 1];
        tasks[i].afterEnd = a[tasks[i].end];
    }
    tasks[maxThreads - 1].end = NUM_ELEMENTS - 1;
    tasks[maxThreads - 1].afterEnd = a[NUM_ELEMENTS - 1];

    for (auto i = 0; i < maxThreads; ++i) {
        std::printf("Task %d:\n\tbegin %d\n\tend %d\n\tbeforeBegin %lf\n\tafterEnd %lf\n", i,
            tasks[i].begin, tasks[i].end, tasks[i].beforeBegin, tasks[i].afterEnd);
    }

    // execute tasks
#pragma omp parallel shared(a, tasks)
    {
        auto threadId = omp_get_thread_num();
        auto const &task = tasks[threadId];
        auto prevOriginal = task.beforeBegin;
        for (auto i = task.begin; i < task.end - 1; ++i) {
            auto tmp = a[i];
            a[i] = (prevOriginal * a[i] * a[i + 1]) / 3.0;
            prevOriginal = tmp;
        }
        a[task.end - 1] = (task.afterEnd * a[task.end - 1] * prevOriginal) / 3.0;
    }

    // get the ground truth
    auto groundTruth = ArrayType{};
    groundTruth[0] = 0;
    groundTruth.back() = groundTruth.size() - 1;
    for (auto i = 1; i < groundTruth.size() - 1; ++i) {
        groundTruth[i] = ((double)(i - 1) * i * (i + 1)) / 3.0;
    }

    // compare muti-threaded result with ground truth
    double maxDiff = std::numeric_limits<double>::epsilon();
    for (auto i = 0; i < groundTruth.size(); ++i) {
        maxDiff = std::max(std::abs(a[i] - groundTruth[i]), maxDiff);
    }

    std::printf("Max diff detween final result and ground truth is %lf\n", maxDiff);

    return 0;
}
